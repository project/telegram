### About

This module allows tp send and to receive messages with Drupal using Telegram protocol.

### Documentation

* See https://telegram.org/
* See https://core.telegram.org/api
* See https://github.com/php-telegram-bot/core

### Maintainers

* Nikolay Lobachev https://www.drupal.org/u/lobsterr
* Jose Reyero https://www.drupal.org/u/jose-reyero

